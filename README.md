# ČSFD Film taste comparison tool

Pri hodnotení filmov na ČSFD používam nasledovný kľúč:

+ \*\*\*\*\* Úplne skvelý film.
+ \*\*\*\* Veľmi dobrý film.
+ \*\*\* "Meh". Dá sa, ale nemôžem úplne odporúčať.
+ \*\* Slabota.
+ \* Bod za snahu.
+ Odpad!

Inými slovami, filmy, ktoré ohodnotím aspoň 4 hviezdičkami, považujem za veľmi podarené. Predpokladám, že podobne k tomu pristupujú aj ostatní používatelia.

Táto aplikácia funguje tak, že zo stránok ČSFD stiahne 2 množiny filmov (pre 2 zadaných používateľov) a spočíta prienik týchto množín => čiže spočíta, koľko filmov obaja používatelia ohodnotili aspoň 4 hviezdičkami.

Požiadavky
----------

+ Python 3 https://www.python.org/
+ BeautifulSoup 4 https://www.crummy.com/software/BeautifulSoup/

Používanie
----------

_python compare.py User-ID1 User-ID2 [output_file.txt]_

User-ID sú v tvare "číslo-prezývka". Napríklad moje vlastné User-ID je 320603-helmiczka. Ak by ste chceli porovnať môj vkus a vkus najviac obľúbeného užívateľa na ČSFD (toho času je to "golfista"), spusťte skript ako:

_python compare.py 320603-helmiczka 95-golfista_

Výstupom bude niečo ako:

The user 320603-helmiczka has 375 top-rated films.  
The user 95-golfista has 2750 top-rated films.  
Both users have 217 top films in common.  
That means:  
320603-helmiczka is 58 % compatible to 95-golfista, while  
95-golfista is 8 % compatible to 320603-helmiczka.  
Their names are:  
500 dní se Summer  
Muži v černém  
Dobyvatelé ztracené archy  
Útěk do divočiny  
Kokosy na sněhu  
Poslední samuraj  
Červený trpaslík  

---

Requirements
------------

+ Python 3 https://www.python.org/
+ BeautifulSoup 4 https://www.crummy.com/software/BeautifulSoup/

Usage
-----

_python compare.py User-ID1 User-ID2 [output_file.txt]_

The script takes two mandatory parameters - ČSFD User IDs. The optional third argument is the output file. User-IDs are in format "number-nickname".  
For instance, if you want to compare my taste to the most liked user (golfista), run the script as:

_python compare.py 320603-helmiczka 95-golfista_

You will see something like:

The user 320603-helmiczka has 375 top-rated films.  
The user 95-golfista has 2750 top-rated films.  
Both users have 217 top films in common.  
That means:  
320603-helmiczka is 58 % compatible to 95-golfista, while  
95-golfista is 8 % compatible to 320603-helmiczka.  
Their names are:  
500 dní se Summer  
Muži v černém  
Dobyvatelé ztracené archy  
Útěk do divočiny  
Kokosy na sněhu  
Poslední samuraj  
Červený trpaslík  
...

All the script does is that it takes 4*-rated and 5*-rated films from both users and computes the intersection of the both sets.

License
------------

All code is open source and licensed under the MIT license. Check the LICENSE file for more information.
