# Copyright (c) 2018 Samuel Mudrík
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import sys
import requests
from bs4 import BeautifulSoup
import multiprocessing

topFilmRating = "*"*4

# Needed because of the Windows command line encoding
def unicodePrint(string, file=None):
    print(string.encode('utf8').decode(file.encoding), file=file)

class TasteComparisonSummary:
    id1 = ""
    id2 = ""
    set1 = set()
    set2 = set()
    commonTopFilms = set()
    commonTopFilmsCount = 0
    id1_to_id2_score = 0
    id2_to_id1_score = 0
    
    def __init__(self, id1, id2):
        self.id1 = id1
        self.id2 = id2
    
    def calculateMatch(self):
        self.commonTopFilms = self.set1.intersection(self.set2)
        self.commonTopFilmsCount = len(self.commonTopFilms)
        if len(self.set1) != 0:
            self.id1_to_id2_score = self.commonTopFilmsCount/len(self.set1) * 100
        if len(self.set2) != 0:
            self.id2_to_id1_score = self.commonTopFilmsCount/len(self.set2) * 100
    
    def getVennChartURL(self):
        norm = max(len(self.set1), len(self.set2))
        
        A_size = str(len(self.set1) / norm)
        B_size = str(len(self.set2) / norm)
        intersection_size = str(self.commonTopFilmsCount / norm)
        
        return ("https://chart.googleapis.com/chart?"
                "cht=v&chs=350x100&"
                "chd=t:" + A_size + "," + B_size + ",0," + intersection_size +
                "&chdl=" + self.id1 + "|" + self.id2)

class CSFD_Compare:
    set1 = set()
    set2 = set()
    outputFile = sys.stdout
    
    def print(string):
        unicodePrint(string, CSFD_Compare.outputFile)
    
    @staticmethod
    def isTopFilm(film):
        rating = film.find("img")
        if not rating:
            return False
        else:
            return topFilmRating in rating.get("alt")
    
    def setOutputFile(self, file):
        outputFile = file
    
    @staticmethod
    def getHTMLPage(url):
        source = requests.get(url)
        return BeautifulSoup(source.text, "html.parser")
    
    @staticmethod
    def getTable(page):
        return page.find("table", { "class" : "ui-table-list" })
    
    @staticmethod
    def fetchTopFilms(userId):
        topFilms = set()
        nextPageNeeded = True
        i = 1

        while nextPageNeeded:
            url = "https://www.csfd.cz/uzivatel/" + userId + "/hodnoceni/podle-rating/strana-" + str(i) + "/"
            i += 1
            table = CSFD_Compare.getTable(CSFD_Compare.getHTMLPage(url))
            if table:
                for film in table.tbody.findAll("tr"):
                    filmName = film.td.a.text
                    if CSFD_Compare.isTopFilm(film):
                        topFilms.add(filmName)
                    else:
                        nextPageNeeded = False
            else:
                break

        return topFilms

    def compare(self, id1, id2):
        summary = TasteComparisonSummary(id1, id2)
        
        pool = multiprocessing.Pool(processes=2)
        pool_outputs = pool.map(CSFD_Compare.fetchTopFilms,[id1, id2])
        pool.close()
        pool.join()
        (summary.set1, summary.set2) = pool_outputs
        
        summary.calculateMatch()
        
        return summary
    
    def printResults(self, summary):
        CSFD_Compare.print("The user " + id1 + " has " + str(len(summary.set1)) + " top-rated films")
        CSFD_Compare.print("The user " + id2 + " has " + str(len(summary.set2)) + " top-rated films")
        CSFD_Compare.print("Both users have " + str(len(summary.commonTopFilms)) + " top films in common.")
        CSFD_Compare.print("That means:")
        CSFD_Compare.print(id1 + " is " + '{:0.0f}'.format(summary.id1_to_id2_score) + " % compatible to " + id2 + ", while ")
        CSFD_Compare.print(id2 + " is " + '{:0.0f}'.format(summary.id2_to_id1_score) + " % compatible to " + id1 + ".")
        CSFD_Compare.print("Their names are: ")
        for film in summary.commonTopFilms:
            CSFD_Compare.print(film)
    

if __name__ == "__main__":
    (id1, id2) = sys.argv[1:3]
    Compare = CSFD_Compare()
    outputFile = open(sys.argv[3], encoding='utf-8', mode='w') if len(sys.argv) > 3 else sys.stdout
    Compare.setOutputFile(outputFile)
    Compare.printResults(Compare.compare(id1, id2))
